<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
	<head>
    	<link rel="stylesheet" type="text/css" href="style.css"/>
	</head>
	<body>
		<header>Telefony w bazie</header>
			<div class = 'flex-container'>
				<xsl:for-each select="phones/phone">
					
						<div class = 'flex-container2'>
							<xsl:attribute name="style">
							<xsl:if test="stock &lt; 1">
                                    background-color: red
                                                </xsl:if>
                                            </xsl:attribute>
							<div class = 'text'>
								<table>
								<tr>Nazwa: <xsl:value-of select="name" /> </tr>
								<tr>Producent: <xsl:value-of select="producer" /> </tr>
								<tr>Cena: <xsl:value-of select="price" /> </tr>
								<tr>Kolor: <xsl:value-of select="color" /> </tr>
								<tr>Pamiec: <xsl:value-of select="memory" /> </tr>
								<tr>System: <xsl:value-of select="system" /> </tr>
								</table>
							</div>
							<div class = 'zdj'>
								<img id="image">
								   <xsl:attribute name="src">
										<xsl:value-of select="image" />
									</xsl:attribute>
								</img>
							</div>
						</div>
				
	     		</xsl:for-each>
	     	</div>
    </body>
</html>
</xsl:template>
</xsl:stylesheet>
